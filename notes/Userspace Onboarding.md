# Userspace Onboarding

https://docs.google.com/document/d/1A_JL5P5kDtcYt2DTHxVrWC3WCW3JadD5oqvwd91UXTo/edit?pli=1

- [cee-labs-flex-08.cee.ral3.lab.eng.rdu2.redhat.com](cee-labs-flex-08.cee.ral3.lab.eng.rdu2.redhat.com)
- [Big ass system to test shellaconda stuff](https://cee-labs-flex-08-drac.mgmt.cee.ral3.lab.eng.rdu2.redhat.com/restgui/start.html?login)
- [And the beaker system link](https://beaker.engineering.redhat.com/view/cee-labs-flex-08.cee.ral3.lab.eng.rdu2.redhat.com#details)

## FIPS

### Overview 

- Federal Information Processing 
- General set of rules that define what is acceptable and unaccetable in terms of security in Linux
- Enable `fips=1`
- [How can I make RHEL FIPS compliant?](https://access.redhat.com/solutions/137833)
- Applications are FIPS aware sometimes, check `/etc/crypto-policies/back-ends/` to see which are aware
  - These point to text files that lists the cryto algorithms available.
  - With FIPS enabled, these files will have far fewer algorithms. 

### Problems

#### FIPS not enabled at boot; 
- Encrypted disk via LUKS using the default algorithm -> reinstall
- Otherwise (like ssh keys) -> turn off, change config, turn on

#### Openscaps scanner
- Update to openscaps where the cipher/algorithm file is different `/etc/crypto-policies/back-ends/` -> breaks scaps
  - [Example Jira](https://issues.redhat.com/browse/RHEL-29684)

## OPENSCAP

- Security Content Automation Protocol
- Static scanning tool that checks for most anything depending on configs 
- Uses XML files to check how to scan and what to scan 
- Scans we do are [here](https://www.redhat.com/security/data/oval/v2/)
- `oscap oval <xml file>`

### Problems

- Scans fail when they should succeed
  - add `--debug` for verbose output to see what's up 
  - reproduce if possible 

## Appcore

### Overview

- Like kdump and crash all in one, grabs a cores, can step through cores and a live application
- 


### Setup a system for reviewing an appcore

1. Enable debuginfo repositories
2. Run `gdb <binary>`
  - It will possibly fail but will tell you what to run to install the debuginfo packages
3. Install debuginfo `dnf debuginfo-install blah`
4. Run it again `gdb <binary>`
5. Set a breakpoint (usually main) `b main`

### Tips

- `layout` will walk you through the lines of code or assembly

### Supplementary Docs

- [Using appcore](https://docs.google.com/document/d/1zwnOwswALRdtHNE6PHj8GYTMdhFJaJxFb0jtK4LMQh4/edit)
- [Example cores](https://gitlab.cee.redhat.com/rmetrich/appcore-prep-integration/-/tree/main)
- [Renaud training](https://drive.google.com/file/d/1GCn8aQpHiRckx0BcfKj0G9vzGMCNIz2J/view)

## SELinux 

- `ausearch -m avc -i -if <file>`
- `fixfiles verify`(prints if a file doesn't have the right)

## Strace

- `strace -Tttvfyys`

## Common First Steps in troubleshooting

- `rpm -Va` 

  - When "commands don't work" as expected, a package won't install or has weird dependencies that already exist on the system, etc, this can confirm that the package is not missing any binaries or other necessary things. As such, we are sanity checking everything that provides immediate functionality to the problem command/package/etc. 
  - We generally don't care about any of the categories of marked files (`c`, `d`, `g`, `l`, `r`) *unless* the `c` is `missing` in the columns. This is largely because these files don't necessarily dictate outright functionality. 


## Supplementary Docs

- [Chapter 7. Scanning the system for configuration compliance and vulnerabilities](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/security_hardening/index#vulnerability-scanning_scanning-the-system-for-configuration-compliance-and-vulnerabilities)

## 

# Homework 
- Re-setup VMs
- Enable FIPS
- Enable fips and break shit
- Use appcore examples
