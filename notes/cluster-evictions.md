# Troubleshooting Cluster Evictions


## Oracle

In a vmcore, the `cssmonitor` might print some stuff to memory about an eviction; 

```
crash> search -c "cssmonit,L"
ffff972540cadc7a: cssmonit,L-xxxx-xx-xx-xx:xx:xx.xxx,oracssdmonitor is abo

--

crash> rd ffff972540cadc7a 14
ffff972540cadc7a:  74696e6f6d737363 2d343230322d4c2c   cssmonit,L-xxxx-
ffff972540cadc8a:  39302d36312d3930 352e31313a35313a   xx-xx-xx:xx:xx.x
ffff972540cadc9a:  736361726f2c3134 6f74696e6f6d6473   xx,oracssdmonito
ffff972540cadcaa:  6f62612073692072 6572206f74207475   r is about to re
ffff972540cadcba:  69687420746f6f62 732065646f6e2073   boot this node s
ffff972540cadcca:  7469202c65636e69 746f6e2064696420   ince, it did not
ffff972540cadcda:  6576696563657220 68206c61636f6c20    receive local h
```