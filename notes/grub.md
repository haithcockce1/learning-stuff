# GRUB

## Notes cuz this shit changes literally every major release

### General Bits
#### UEFI or Legacy?
- GRUB boots based on either BIOS (legacy) or UEFI
	- For UEFI, there's `/boot/efi/EFI/redhat/`
	- For Legacy, there's `/boot/grub2`
	- For ease of use, both will be referred to as the grub boot directory.
- `dmesg` can be checked for this.  `grep -i uefi sos_commands/kernel/dmesg -c`
	- If 0, then Legacy, if non-zero, UEFI.

### RHEL 7

### RHEL 8
- A `grub.cfg` and `grubenv` both must exist in the same grub boot directory
- You _need_ a symlink from `/etc/grub2*` to the `grub.cfg` file in the grub boot directory

#### Legacy
Symlink 
```
# ll /etc/grub2.cfg
lrwxrwxrwx. 1 root root 22 Feb 21 08:12 /etc/grub2.cfg -> ../boot/grub2/grub.cfg
```
#### UEFI
Symlink
```
$ ll etc/grub2-efi.cfg lrwxrwxrwx. 1 root root 31 Nov 4 2021 etc/grub2-efi.cfg -> ../boot/efi/EFI/redhat/grub.cfg
```
#### Common Issues
_Defaut kernel does not update. The system boots fine but in older kernels only._
- Usually the result of `grubenv` being in a weird state. This also means `grub.cfg` is/should be fine.
- _Resolution_ 
	- Check to see if a `grubenv` exists and is valid. If so move it to the appropriate place and rebuild `grub2-mkconfig`
	- You may be able to simply run `grub2-mkconfig`
### RHEL 9