# Permissions

- Check for fapolicy 
- Check for selinux
- Check for third party security modules
- Check `/etc/nsswitch.conf` for third party entries for logins
- Check if it happens with root as well
- Check not just the group and user _but also_ the UID and GID. 
    - You can have multiple UID and GID mapped to the same user and group for unix. 
    - active directory can lead to this 
    - `getent group | grep ^oinstall` e.g.

- Check the file and directories it is in and above
    - `ls -la`
    - `ls -ld`
    - `stat`
    - `lsattr`
    - `getfacl`
- Maybe check `rpm -Va` as well