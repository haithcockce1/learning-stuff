# RPM 

## Verifying

- `rpm -V`

    ```
    rpm {-V|--verify} [select-options] [verify-options]

    Verifying a package compares information about the installed files in the package  with
    information about the files taken from the package metadata stored in the rpm database.
    Among other things, verifying compares the size, digest, permissions, type,  owner  and
    group  of  each  file.  Any discrepancies are displayed.  Files that were not installed
    from the package, for example, documentation files excluded on installation  using  the
    "--excludedocs" option, will be silently ignored.
    [...]
    The format of the output is a string of 9 characters, a possible attribute marker:

    c %config configuration file.
    d %doc documentation file.
    g %ghost file (i.e. the file contents are not included in the package payload).
    l %license license file.
    r %readme readme file.

    from the package header, followed by the file name.  Each of the 9  characters  denotes
    the  result  of  a  comparison of attribute(s) of the file to the value of those attri‐
    bute(s) recorded in the database.  A single "." (period) means the test passed, while a
    single "?" (question mark) indicates the test could not be performed (e.g. file permis‐
    sions prevent reading). Otherwise,  the  (mnemonically  emBoldened)  character  denotes
    failure of the corresponding --verify test:


    S file *S*ize differs
    M *M*ode differs (includes permissions and file type)
    5 digest (formerly MD*5* sum) differs
    D *D*evice major/minor number mismatch
    L read*L*ink(2) path mismatch
    U *U*ser ownership differs
    G *G*roup ownership differs
    T m*T*ime differs
    P ca*P*abilities differ
    ```

## Corruption

- An empty list of package generally means the /var/lib/rpm/Packages file was somehow damages or unavailable to RPM (due to being deleted, moved, renamed, etc) and then an RPM or YUM command was ran after the fact. From there, RPM rebuilds the database based off of the (potentially non-existent) Packages file into an empty RPM database. 


## Leftover files

- Fun fact, you can have files just chilling there even after you remove them. `rpm -e <PKG>.rpm --justdb` removes it from the database and not the actual system.


## Common issues;

#### `cannot install both <package> and <package>`
- This can be for many reasons. If the customer is doing `dnf update --security` one package could be in the security update path and the other is not but dependent on it. Find the source package (`rpm -qi <PROBLEM PACKAGE> | grep Source`) and update that my itself. From there, re-attempt. 