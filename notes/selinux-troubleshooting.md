# SELinux Troubleshooting

## General Support Delivery Workflow

1. Confirm the context type (aka just the context) is what is expected (via checking avc denial/fcontext against a default installation)
2. Rule out third party security stuff including Active Directory stuff
3. Confirm allowance rules are fine (`sesearch -A -s <sourceContext> -t <targetContext> -c <class>`)
4. Then it may be a bug 