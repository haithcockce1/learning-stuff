# Dynamic User

## Overview

- Dynamic users are users systemd can create on the fly
- In constrast, most users are statically defined users
- The user is automatically allocated from the UID range 61184–65519, by looking for a so far unused UID

#### Sticky users

- UIDs and GIDs often remain even on files after user/group removal 
- Dynamic users, by being ephemeral, can not have their UIDs and GIDs hanging around. 
- Thus Dynamic users can either not be allowed to write to files at all or write to ephemeral files specific to the user

## Implementation

- Service unit files may include `DynamicUser=yes` in the `[Service]` section. 
- Doing so implies
    - `ProtectSystem=strict` and `ProtectHome=read-only`: turn off write access to pretty much the whole OS directory tree, with a few relevant exceptions, such as the API file systems `/proc`, `/sys` and so on, as well as `/tmp` and `/var/tmp`.
    - `PrivateTmp=yes`: This option sets up `/tmp` and `/var/tmp` for the service in a way that it gets its own, disconnected version of these directories, that are not shared by other services, and whose life-cycle is bound to the service's own life-cycle.
    - `RemoveIPC=yes`: ensures that when the service goes down all SysV and POSIX IPC objects (shared memory, message queues, semaphores) owned by the service's user are removed.
- Note the above would be a more ephemeral filesystem contents.

#### Persistent Data

- Systemd creates a boundary directory under the various directories it needs to protect when enabling persistent data for the dynamic user wherein everything is owned by that UID:GID.
- E.g. with `StateDirectory=foobar`, `/var/lib/foobar/` is created and owned by the service's user and persists. 
- E.g. with `StateDirectory=foobar` *and* `DynamicUser=yes`, `/var/lib/private/foobar` is created and owned by the dynamic user where `/var/lib/private/` is owned by `root` with `0700` permissions.

## Sources

- [Dynamic Users with systemd](https://0pointer.net/blog/dynamic-users-with-systemd.html)